/*
 * Copyright 2010, 2011 Institut Pasteur.
 * 
 * This file is part of ICY.
 * 
 * ICY is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ICY is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ICY. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.fab.trackmanager;

import icy.sequence.Sequence;

import java.util.ArrayList;

import plugins.nchenouard.spot.Detection;

public class TrackGroup
{
    private final Sequence sequence;
    /** 
     * DO NOT DIRECTLY ADD OR REMOVE tracks from this trackSegmentList. Always use getters and adder/setters
     * */
    private ArrayList<TrackSegment> trackSegmentList;

    public TrackGroup(Sequence sequence)
    {
        this.sequence = sequence;
        trackSegmentList = new ArrayList<TrackSegment>();
    }

    /**
     * @return the sequence
     */
    public Sequence getSequence()
    {
        return sequence;
    }

    public ArrayList<TrackSegment> getTrackSegmentList()
    {
        return trackSegmentList;
    }

    public void addTrackSegment(TrackSegment ts)
    {
        if (ts.getOwnerTrackGroup() != null)
        {
            System.err.println("The trackSegment is already owned by another TrackGroup.");
            return;
        }
        
        //System.out.println("Track segment added to group. TS: " +ts.toString() );
        
        ts.setOwnerTrackGroup(this);
        trackSegmentList.add(ts);
    }

    public TrackSegment getTrackSegmentWithDetection(Detection detection)
    {

    	ArrayList<TrackSegment> trackSegmentList = getTrackSegmentList();

    	for ( TrackSegment ts : trackSegmentList )
    	{
    		if ( ts.containsDetection( detection ) ) return ts;
    	}

    	return null;
    }
    
    public void clearAllTrackSegment()
    {
    	ArrayList<TrackSegment> trackSegmentListCopy = new ArrayList<TrackSegment>( trackSegmentList );    	

    	for ( TrackSegment ts: trackSegmentListCopy )
    	{
    		removeTrackSegment( ts );
    	}
    }
    
    public void removeTrackSegment(TrackSegment ts)
    {
    	// should remove links
        ts.setOwnerTrackGroup(null);
        ts.removeAllLinks();
        trackSegmentList.remove(ts);        
    }

    private String description;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return description;
    }

}
